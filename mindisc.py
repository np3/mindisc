import random

# Smallest enclosing circle problem
# 2014, Nick Petrov

class Disc:
  # Inside disc tests implementation
  class DistChecker:
    def __init__(self, obj):
      self.obj = obj
    def __call__(self, p):
      distSqr = self.obj.calcDistSqr(self.obj.center, [2 * x for x in p])
      return  distSqr <= self.obj.diamSqr
  class SignChecker:
    def __init__(self, obj):
      self.obj = obj
    def __call__(self, p):
      return self.obj.areaSign * self.obj.sign(self.obj.findDet4x4(p)) <= 0

  def calcDistSqr(self, p1, p2):
    return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1])
  def calcDet3x3(self, m):
    return m[0] * (m[4] * m[8] - m[7] * m[5]) - m[1] * (m[3] * m[8] - m[6] * m[5]) + m[2] * (m[3] * m[7] - m[6] * m[4])
  def findDet4x4(self, p):
    firstRow = [p[0], p[1], (p[0] * p[0] + p[1] * p[1]), 1]
    return sum(p * q for p, q in zip(firstRow, self.matBlocks))
  def sign(self, x):
    return 1 if x > 0 else -1 if x < 0 else 0
  def makeMatBlocks(self, p1, p2, p3):
    # 
    firstSqr = p1[0] * p1[0] + p1[1] * p1[1]
    secondSqr = p2[0] * p2[0] + p2[1] * p2[1]
    thirdSqr = p3[0] * p3[0] + p3[1] * p3[1]
    # Fill blocks
    firstBlock = [p1[1], firstSqr, 1, p2[1], secondSqr, 1, p3[1], thirdSqr, 1]
    secondBlock = [p1[0], firstSqr, 1, p2[0], secondSqr, 1, p3[0], thirdSqr, 1]
    thirdBlock = [p1[0], p1[1], 1, p2[0], p2[1], 1, p3[0], p3[1], 1]
    fourthBlock = [p1[0], p1[1], firstSqr, p2[0], p2[1], secondSqr, p3[0], p3[1], thirdSqr]
    
    return [self.calcDet3x3(firstBlock), -self.calcDet3x3(secondBlock), self.calcDet3x3(thirdBlock), -self.calcDet3x3(fourthBlock)]
  def isCollinear(self, p1, p2, p3):
    return self.calcDet3x3([p1[0], p1[1], 1, p2[0], p2[1], 1, p3[0], p3[1], 1]) == 0
  def makeFrom2Points(self, p1, p2, ind):
    self.center = [p1[0] + p2[0], p1[1] + p2[1]]
    self.diamSqr = self.calcDistSqr(p1, p2)
    self.testFunc = Disc.DistChecker(self)
    self.indList = ind
  def makeFrom3Points(self, p1, p2, p3, ind):
    self.areaSign = self.sign(self.calcDet3x3([p1[0], p1[1], 1, p2[0], p2[1], 1, p3[0], p3[1], 1]))
    self.matBlocks = self.makeMatBlocks(p1, p2, p3)
    self.testFunc = Disc.SignChecker(self)
    self.indList = ind
  def __init__(self):
    self.center = []
    self.diamSqr = 0
    self.areaSign = 1
    self.matBlocks = []
    self.indList = []
    pass
  def isInside(self, p):
    #if self.center:
    #  return 4 * calcDistSqr(self.center, [2 * x for x in p]) <= self.diamSqr # Integer numbers...
    #else:
    #  return self.areaSign * sign(findDet4x4(p)) <= 0
    return self.testFunc(p)
  def __str__(self):
    return " ".join(str(x) for x in self.indList)

def doPerm(perm):
    for i in range(1, len(perm)):
        # Use Mersenne Twister PRNG (default in Python)
        randI = random.randint(0, i - 1)
        perm[i], perm[randI] = perm[randI], perm[i]
    return perm

# P - point list
# S - indices of bound points
# I - current points indices
def minDiscImpl(P, S, I):
  curDisc = Disc()
  if len(S) == 3:
    curDisc.makeFrom3Points(P[S[0]], P[S[1]], P[S[2]], S)
    return curDisc
  else:
    perm = doPerm(I)

    if len(S) == 2:
        curDisc.makeFrom2Points(P[S[0]], P[S[1]], S)
    elif len(S) == 1:
        curDisc.makeFrom2Points(P[S[0]], P[perm[0]], [S[0], perm[0]])
    else:
        curDisc.makeFrom2Points(P[perm[0]], P[perm[1]], [perm[0], perm[1]])
    # Check our point set
    # assert len(S) <= 3, 'S size <= 3'
    for i in range(2 - len(S), len(I)):
      ind = perm[i]
      if curDisc.isInside(P[ind]):
        continue
      else:
        # Copy border indices to avoid changes of original array
        SS = S[:]
        SS.append(ind)
        curDisc = minDiscImpl(P, SS, I[:i])
    return curDisc

def minDisc(P):
    if len(P) <= 1:
        return Disc()
    return minDiscImpl(P, [], range(len(P)))

if __name__ == "__main__":
  # read points from stdin
  input_data = map(int, raw_input().split())
  points = [input_data[i:i+2] for i in range(0, len(input_data), 2)]
  print minDisc(points)